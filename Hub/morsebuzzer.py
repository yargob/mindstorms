# morsebuzzer // 2020-10-31/HB9KNS

# left button: select mode
# right button: execute etc

from mindstorms import MSHub, Motor, MotorPair, ColorSensor, DistanceSensor, App
from mindstorms.control import wait_for_seconds, wait_until, Timer
from mindstorms.operator import greater_than, greater_than_or_equal_to, less_than, less_than_or_equal_to, equal_to, not_equal_to
import math

speedunit = 0.1
dit = 0.1
maxplay = 3
beeptone = 80

hub = MSHub()
timer = Timer()

hub.status_light.on('orange')
# modes: 0:beep            1:blink    2:both          3:torch    4:play      5:record
disps = [' MUSIC_CROTCHET',' DIAMOND',' MUSIC_QUAVER',' SQUARE', ' ARROW_E', ' ARROW_S']
m_beep = 0
m_blink = 1
m_both = 2
m_torch = 3
m_play = 4
m_record = 5
mode = m_torch
torch = False
play = 0
record = '- . ... -'

def change_mode(mode):
    mode += 1
    if mode>=len(disps):
        mode = 0
    d = disps[mode]
    if d[0:1] == ' ':
        hub.light_matrix.show_image(d[1:],100)
    else:
        hub.light_matrix.write(d)
    return mode

def blink(delay=0):
    for x in range(5):
        for y in range(5):
            hub.light_matrix.set_pixel(x,y,100)
            if delay>0:
                wait_for_seconds(delay/100)

def toggle_torch(torch):
    if torch:
        torch = False
        hub.status_light.off()
        hub.light_matrix.off()
    else:
        torch = True
        hub.status_light.on('white')
        blink(1)
    return torch

def loop_through(val,maxval=1):
    if val<maxval:
        val += 1
        hub.light_matrix.write(str(val))
    else:
        val = 0
        hub.light_matrix.show_image('NO')
    return val

def buzzblink(n,unit=1):
    if mode == m_beep or mode == m_both:
        hub.speaker.start_beep(beeptone)
    if mode == m_blink or mode == m_both:
        blink()
    wait_for_seconds(n*unit)
    hub.speaker.stop()
    hub.light_matrix.off()
    wait_for_seconds(unit)

def play_record(r,speed=1,count=-1):
    hub.status_light.off()
    c = count
    u = speed*speedunit
    while c:
        for i in range(len(r)):
            if hub.right_button.was_pressed():
                go_on = False
                break
            elif r[i] == ' ':
                wait_for_seconds(4*u)
            elif r[i] == '.':
                buzzblink(1,u)
            elif r[i] == '-':
                buzzblink(3,u)
            wait_for_seconds(2*u)
        wait_for_seconds(8*u)
        c -= 1
        if c<0:
            c = -1
    hub.status_light.on('green')

def record_record(dit=0.1):
    hub.light_matrix.show_image('CHESSBOARD')
    hub.right_button.wait_until_released()
    hub.status_light.on('red')
    r = ''
    w = 0
    while not hub.left_button.was_pressed():
        hub.light_matrix.off()
        if hub.motion_sensor.was_gesture('doubletapped'):
            r = r[0:r.rfind(' ')]
            print('r=',r)
        if hub.right_button.was_pressed():
            c = '.'
            hub.light_matrix.set_pixel(2,2)
            wait_for_seconds(dit)
            if hub.right_button.is_pressed():
                c = '-'
                hub.light_matrix.set_pixel(1,2)
                hub.light_matrix.set_pixel(3,2)
                hub.right_button.wait_until_released()
            r += c
            wait_for_seconds(dit)
            if not hub.right_button.is_pressed():
                wait_for_seconds(dit)
                if not hub.right_button.is_pressed():
                    wait_for_seconds(dit)
                    if not hub.right_button.is_pressed():
                        r += ' '
                        w += 1
                        hub.light_matrix.show_image('YES')
            print('r=',r)
        wait_for_seconds(0.01)
    hub.left_button.wait_until_released()
    hub.status_light.on('green')
    hub.light_matrix.write('#'+str(w))
    hub.light_matrix.show_image('YES')
    return r

timer.reset()
while True:
    if hub.right_button.was_pressed():
        if mode == m_torch:
            torch = toggle_torch(torch)
        elif mode == m_play:
            play = loop_through(play,maxplay)
        elif mode == m_record:
            record = record_record(dit)
        elif play:
            play_record(record,play)
        else:
            if mode == m_beep or mode == m_both:
                hub.speaker.start_beep(beeptone)
            if mode == m_blink or mode == m_both:
                blink()
                hub.status_light.on('white')
            hub.right_button.wait_until_released()
            hub.speaker.stop()
            hub.light_matrix.off()
            hub.status_light.off()
        timer.reset()
    if hub.left_button.was_pressed():
        mode = change_mode(mode)
        hub.left_button.wait_until_released()
        hub.status_light.on('green')
        timer.reset()
    wait_for_seconds(0.05)
    if timer.now()>=30 and mode != m_torch:
        hub.light_matrix.off()
        hub.status_light.off()
        hub.light_matrix.set_pixel(2,2,22)
        hub.left_button.wait_until_pressed()
        if hub.left_button.was_pressed():
            pass
        if hub.right_button.was_pressed():
            pass
        timer.reset()
