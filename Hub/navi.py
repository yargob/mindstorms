# navi // 2020-10-28/HB9KNS

from mindstorms import MSHub, Motor, MotorPair, ColorSensor, DistanceSensor, App
from mindstorms.control import wait_for_seconds, wait_until, Timer
from mindstorms.operator import greater_than, greater_than_or_equal_to, less_than, less_than_or_equal_to, equal_to, not_equal_to
import math

hub = MSHub()
hub.status_light.on('green')
hub.motion_sensor.reset_yaw_angle()
x = 0
y = 0
oldx = 0
oldy = 0
offset = -90

while True:
    if hub.left_button.was_pressed():
        hub.left_button.wait_until_released()
        hub.motion_sensor.reset_yaw_angle()
        hub.light_matrix.off()
    rot = hub.motion_sensor.get_yaw_angle()
    oldx = x
    oldy = y
    accx = 2+2.4*math.cos(math.radians(offset-rot))
    accy = 2+2.4*math.sin(math.radians(offset-rot))
    x = int(0.5+accx)
    y = int(0.5+accy)
    if rot == 0:
        hub.light_matrix.set_pixel(2,1,100)
        hub.light_matrix.set_pixel(2,2,100)
        hub.light_matrix.set_pixel(2,3,100)
        hub.status_light.on('green')
    else:
        hub.light_matrix.set_pixel(2,1,0)
        hub.light_matrix.set_pixel(2,2,int(100-30*(abs(x-accx)+abs(y-accy))))
        hub.light_matrix.set_pixel(2,3,0)
        hub.status_light.on('red')
    hub.light_matrix.set_pixel(x,y,100)
    if oldx != x or oldy != y:
        hub.light_matrix.set_pixel(oldx,oldy,0)
    wait_for_seconds(0.1)
