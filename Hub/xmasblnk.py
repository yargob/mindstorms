# feiertagsfenster // 2023/HB9KNS

# left button: select mode
# right button: execute etc

from mindstorms import MSHub, Motor, MotorPair, ColorSensor, DistanceSensor, App
from mindstorms.control import wait_for_seconds, wait_until, Timer
from mindstorms.operator import greater_than, greater_than_or_equal_to, less_than, less_than_or_equal_to, equal_to, not_equal_to
import math

speedunit = 0.3
beeptone = 80

hub = MSHub()
hub.status_light.on('green')
# modes: 0:beep        1:blink    2:both            3:torch    4:play    5:record
disps = [' MUSIC_CROTCHET',' DIAMOND',' MUSIC_QUAVER',' SQUARE', ' GO_RIGHT', ' ARROW_S']
m_beep = 0
m_blink = 1
m_both = 2
m_torch = 3
m_play = 4
m_record = 5
mode = m_torch
torch = False
play = False
# record = [' ','--...',]
# record = ['..-. .-. --- .... .', ' ', '.-- . .. .... -. .- ---- - . -.', ' ',]
record = ['--. ..- - . ...', ' ', '-. . ..- . ...', ' ', '.--- .- .... .-.', ' ', '..--- ...--', ' ',]

def change_mode(mode):
    mode += 1
    if mode>=len(disps):
        mode = 0
#    print('mode',mode,', ',len(disps),'disps')
    d = disps[mode]
    if d[0:1] == ' ':
        hub.light_matrix.show_image(d[1:],100)
    else:
        hub.light_matrix.write(d)
    return mode

def blink(delay=0):
    for x in range(5):
        for y in range(5):
            hub.light_matrix.set_pixel(x,y,100)
            if delay>0:
                wait_for_seconds(delay/100)

def toggle_torch(torch):
    if torch:
        torch = False
        hub.light_matrix.off()
    else:
        torch = True
        blink(1)
    return torch

def toggle_yesno(flag):
    if flag:
        flag = False
        hub.light_matrix.show_image('NO')
    else:
        flag = True
        hub.light_matrix.show_image('YES')
    return flag

def buzzblink(u):
    if mode == m_beep or mode == m_both:
        hub.speaker.start_beep(beeptone)
    if mode == m_blink or mode == m_both:
        blink()
    wait_for_seconds(u*speedunit)
    if mode == m_beep or mode == m_both:
        hub.speaker.stop()
    if mode == m_blink or mode == m_both:
        hub.light_matrix.off()
    wait_for_seconds(speedunit)

def charwait(u=speedunit):
    if speedunit > 0.2:
        wait_for_seconds(3*u)
    else:
        wait_for_seconds(2*u)

def play_record(r):
    hub.light_matrix.show_image('BUTTERFLY')
    wait_for_seconds(8*speedunit)
    go_on = True
    while go_on:
        for i in range(len(r)):
            if r[i] == ' ':
                wait_for_seconds(8*speedunit)
            else:
                for j in range(len(r[i])):
                    if hub.right_button.was_pressed():
                        go_on = False
                        break
                    if r[i][j] == '.':
                        buzzblink(1)
                    if r[i][j] == '-':
                        if speedunit > 0.2:
                            buzzblink(4)
                        else:
                            buzzblink(3)
                    if r[i][j] == ' ':
                        charwait(speedunit)
                charwait(speedunit)
                if not go_on:
                    break

def record_record():
    hub.light_matrix.show_image('SMILE')
    return ['-']

while True:
    if hub.right_button.was_pressed():
        if mode == m_torch:
            torch = toggle_torch(torch)
        elif mode == m_play:
            play = toggle_yesno(play)
        elif mode == m_record:
            record = record_record()
        elif play:
            play_record(record)
            mode -= 1
            change_mode(mode)
        else:
            if mode == m_beep or mode == m_both:
                hub.speaker.start_beep(beeptone)
            if mode == m_blink or mode == m_both:
                blink()
            hub.right_button.wait_until_released()
            hub.speaker.stop()
            hub.light_matrix.off()
    if hub.left_button.was_pressed():
        mode = change_mode(mode)
        hub.left_button.wait_until_released()
    wait_for_seconds(0.05)
